<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'exim');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '<z<_&FjPaI{S)@Y7Onks47=+KzS?cHxPzM^zUL..h$N)?0$_;xc0t*+EyR7DENq_');
define('SECURE_AUTH_KEY',  ')Tr ig=AIXG;FaJJt*H7=j2fLwXWxj(k++D6A1&+j,R,#tfCLndL=}`0>6_-ti4>');
define('LOGGED_IN_KEY',    '?<Qm+zGOooYE}3JJq^(x]^!5s_}D;k0CTY Yo5j:YV4$enQ B:E@ya-Ff6!Ce+!@');
define('NONCE_KEY',        'L}o_ZmuOwPw8<;FB}ddN.1vYn+)2-kjRX7Z%=JMbD||)DilHWaojJV6xy<`+|x-b');
define('AUTH_SALT',        '`<(k)-/!#OC[0mKnTPxarBEF!q?K?D`G; F9+:l;Xk}%%)e,<Wm% WrjW$9DmqG2');
define('SECURE_AUTH_SALT', '+Rt8E. i8:Ot#escUs6%A- HFEp9gmS5Kr-dj$R$XIQkLl@E M%0m7 u PC}}U3%');
define('LOGGED_IN_SALT',   'FWcmtQure`M~n>fG8ECMp;crq<?0l{`3X<kVj!`yz|ybGiyPQBBUS {9-+(@|Wt|');
define('NONCE_SALT',       'M55QJ@|R3F-6kOe|>ovXH47,tY@9QfoD97qAntb;oA:0v|0$4|iJH]X2.Id_#>3N');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
